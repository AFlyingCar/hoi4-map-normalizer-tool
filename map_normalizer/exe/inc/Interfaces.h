
#ifndef INTERFACES_H
# define INTERFACES_H

namespace MapNormalizer {
    int runHeadless();
    int runGUIApplication();

    int runApplication();
}

#endif

